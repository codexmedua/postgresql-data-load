# From https://github.com/NaysanSaran/pandas2postgresql
# GPL V3
from io import StringIO
import psycopg2
import csv

def copy_from_stringio(conn, df, table):
    """
    Here we are going save the dataframe in memory 
    and use copy_from() to copy it to the table
    """
    # save dataframe to an in memory buffer
    buffer = StringIO()
    df.to_csv(buffer,
              index_label='id',
              header=False,
              escapechar="\\",
              sep="\t",
              quoting=csv.QUOTE_NONNUMERIC)
    buffer.seek(0)
    
    cursor = conn.cursor()
    try:
        cursor.copy_expert("COPY "+table+" FROM STDIN (DELIMITER '\t')", buffer)
        conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        print("Error: %s" % error)
        conn.rollback()
        cursor.close()
        return 1
    print("copy_from_stringio() done")
    cursor.close()
    conn.commit()
