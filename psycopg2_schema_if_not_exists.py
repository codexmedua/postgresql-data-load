def schema_if_not_exists(conn,schema):
    """Create schema if it doesn t exist .

    Args:
        conn (Connection): psycopg2 database connection
        schema (string): Schema name
    """
    try:
        cur = conn.cursor()
        cur.execute("""
        CREATE SCHEMA IF NOT EXISTS {schema};
        """.format(schema=schema))
    except:
        pass
    finally:
        cur.close()
        conn.commit()